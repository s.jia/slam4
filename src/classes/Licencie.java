package classes;
public class Licencie {
	
	private String code, nom, prenom, dateNaiss;
	private Club leClub;

	/**
	    * Permet d'obtenir le code d'un licencié
	    * @return - le code du club sous forme de chaine de caractères
	    */
	public String getCode() {
		return code;
	}

	/**
	    * Permet la modification le code d'un licencié
	    * @param titre - le nouveau code d'un licencié à affecter
	    */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	    * Permet d'obtenir le nom d'un licencié
	    * @return - le nom du licencié sous forme de chaine de caractères
	    */
	public String getNom() {
		return nom;
	}

	/**
	    * Permet la modification le nom d'un licencié
	    * @param titre - le nouveau nom d'un licencié à affecter
	    */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	    * Permet d'obtenir le prénom du licencié 
	    * @return - le prénom du licencié sous forme de chaine de caractères
	    */
	public String getPrenom() {
		return prenom;
	}


	/**
	    * Permet la modification le prénom d'un licencié
	    * @param titre - le nouveau prénom d'un licencié à affecter
	    */
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	/**
	    * Permet d'obtenir le prénom du licencié 
	    * @return - la date de naissance du licencié sous forme de chaine de caractères
	    */
	public String getDateNaiss() {
		return dateNaiss;
	}

	/**
	    * Permet la modification la date de naissance d'un licencié
	    * @param titre - la nouvelle date de naissance d'un licencié à affecter
	    */
	public void setDateNaiss(String dateNaiss) {
		this.dateNaiss = dateNaiss;
	}

	/**
	    * Permet d'obtenir le club du licencié 
	    * @return - le club du licencié  sous la classe Club
	    */
	public Club getLeClub() {
		return leClub;
	}

	/**
	    * Permet la modification le club d'un licencié 
	    * @param titre - le nouveau club d'un licencié à affecter
	    */
	public void setLeClub(Club leClub) {
		this.leClub = leClub;
	}

	/**
	    * Construit un Licencie - initialise avec des valeurs par défaut les propriétés de la classe
	    */ 
	public Licencie() {
		this.code = "";
		this.nom = "";
		this.prenom = "";
		this.dateNaiss = "";
		this.leClub = new Club();
	}
	
	/**
	    * Construit un Film - initialise avec des valeurs par défaut les propriétés de la classe
	    * @param code - alimente la propriété code
	    * @param nom - alimente la propriété nom
	    * @param prenom - alimente la propriété prenom
	    * @param dateNaiss - alimente la propriété dateNaiss
	    * @param leClub - alimente la propriété leClub
	    */

	public Licencie(String code, String nom, String prenom, String dateNaiss, Club leClub) {
		this.code = code;
		this.nom = nom;
		this.prenom = prenom;
		this.dateNaiss = dateNaiss;
		this.leClub = leClub;
	}

	@Override
	public String toString() {
		return "Licencie [code=" + code + ", nom=" + nom + ", prenom=" + prenom + ", dateNaiss=" + dateNaiss
				+ ", leClub=" + leClub + "]";
	}
	
	

}
