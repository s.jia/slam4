package classes;

public class Club {


	private String code, nom, nom_president, nom_entraineur;

	/**
	    * Permet d'obtenir le code d'un club
	    * @return - le code du club sous forme de chaine de caractères
	    */
	public String getCode() {
		return code;
	}
	
	/**
	    * Permet la modification le code d'un club
	    * @param titre - le nouveau code d'un club à affecter
	    */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	    * Permet d'obtenir le nom d'un club
	    * @return - le nom du club sous forme de chaine de caractères
	    */
	public String getNom() {
		return nom;
	}

	/**
	    * Permet la modification le nom d'un club
	    * @param titre - le nouveau nom d'un club à affecter
	    */
	public void setNom(String nom) {
		this.nom = nom;
	}

	/**
	    * Permet d'obtenir le nom du président d'un club
	    * @return - le nom du président du club sous forme de chaine de caractères
	    */
	public String getNom_president() {
		return nom_president;
	}

	/**
	    * Permet la modification le nom du président d'un club
	    * @param titre - le nouveau nom du président d'un club à affecter
	    */
	public void setNom_president(String nom_president) {
		this.nom_president = nom_president;
	}

	/**
	    * Permet d'obtenir le nom de l'entraineur d'un club
	    * @return - le nom de l'entraineur du club sous forme de chaine de caractères
	    */
	public String getNom_entraineur() {
		return nom_entraineur;
	}

	/**
	    * Permet la modification le nom de l'entraineur d'un club
	    * @param titre - le nouveau nom de l'entraineur d'un club à affecter
	    */
	public void setNom_entraineur(String nom_entraineur) {
		this.nom_entraineur = nom_entraineur;
	}
	
	/**
	    * Construit un club - initialise avec des valeurs par défaut les propriétés de la classe
	    */ 
	public Club() {
		this.code = "";
		this.nom = "";
		this.nom_president = "";
		this.nom_entraineur = "";
	}

	 /**
	    * Construit un club avec paramètres alimentant les propriétés
	    * @param code - alimente la propriété code
	    * @param nom - alimente la propriété nom
	    * @param nom_president - alimente la propriété nom_president
	    * @param nom_entraineur - alimente la propriété nom_entraineur
	    */
	public Club(String code, String nom, String nom_president, String nom_entraineur) {
		this.code = code;
		this.nom = nom;
		this.nom_president = nom_president;
		this.nom_entraineur = nom_entraineur;
	}

	@Override
	public String toString() {
		return "Club [code=" + code + ", nom=" + nom + ", nom_president=" + nom_president + ", nom_entraineur="
				+ nom_entraineur + "]";
	}



}
