package tests;

import classes.Club;
import classes.Licencie;
import dao.ClubDAO;
import dao.DAO;
import dao.LicencieDAO;

public class TestDAO {

	public static void main() {
		// définition des objets d’accès aux données
		DAO<Licencie> licencie = new LicencieDAO();
		DAO<Club> club = new ClubDAO();
		// recherche d'un licencié en fonction de son code (lecture / Read)
		System.out.println(licencie.read("TAGe").toString());
		// recherche d'un club en fonction de son code (lecture / Read)
		Club unClub = club.read("C1");
		// insertion d'un nouveau licencié (ajout / création / Create)
		Licencie unLicencie = new Licencie("CACl", "CALVIN","Clémence","17/05/1990",
		unClub);
		licencie.create(unLicencie);
		// insertion d'un nouveau club (ajout / création / Create) .../...
		Club unClub1 = new Club("CACl", "CALVIN","Clémence","17/05/1990");
		// maj d’un licencié (Update) .../...
		licencie.update(unLicencie);
		// maj d’un club (Update) .../...
		club.update(unClub1);
		// suppression d’un licencié (Delete) .../...
		licencie.delete(unLicencie);
		// suppression d’un club (Delete) .../...
		club.delete(unClub1);
		// récupérer tous les clubs et les afficher .../...
		System.out.println(club.recupAll());
		// récupérer tous les licenciés et les afficher .../...
		System.out.println(licencie.recupAll());
		}

}
