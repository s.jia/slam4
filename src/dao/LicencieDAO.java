package dao;

import java.sql.*;
import java.util.*;

import classes.Licencie;

//définition de la classe d'accès aux données de la table mvc.licencie
public class LicencieDAO extends DAO<Licencie> {

	@Override
	public List<Licencie> recupAll() {
		// définition de la liste qui sera retournée en fin de méthode
		List<Licencie> listeClubs = new ArrayList<Licencie>(); 
		
		// déclaration de l'objet qui servira pour la requète SQL
		try {
			Statement requete = this.connect.createStatement();

			// définition de l'objet qui récupère le résultat de l'exécution de la requète
			ResultSet curseur = requete.executeQuery("select * from \"mvc\".club");

			// tant qu'il y a une ligne "résultat" à lire
			while (curseur.next()){
				// objet pour la récup d'une ligne de la table Club
				Licencie unLicencie = new Licencie();
				
				unLicencie.setCode(curseur.getString("code"));
				unLicencie.setNom(curseur.getString("nom"));
				unLicencie.setPrenom(curseur.getString("prenom"));
				unLicencie.setDateNaiss(curseur.getString("date de naissance"));
				unLicencie.setLeClub(curseur.getClub("club"));
				
				listeClubs.add(unLicencie);
			}
					
			curseur.close();
			requete.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} 

		return listeClubs; 
	}
	
	
	// Insertion d'un objet licencie dans la table licencie (1 ligne)
	@Override
	public void create(Licencie obj) {
		try {
			 	PreparedStatement prepare = this.connect
	                                        .prepareStatement("INSERT INTO \"mvc\".licencie VALUES(?, ?, ?, ?, ?)"
	                                         );
			 	prepare.setString(1, obj.getCode());
				prepare.setString(2, obj.getNom());
				prepare.setString(3, obj.getPrenom());
				prepare.setString(4, obj.getDateNaiss());
				prepare.setClub(5, obj.getClub());
					
				prepare.executeUpdate();  
					
			}
		catch (SQLException e) {
		e.printStackTrace();
		} 
	}
		
		
	// Recherche d'un licencie par rapport à son code	
	@Override
	public Licencie read(String code) {
	
		Licencie leLicencie = new Licencie();
		try {
	          ResultSet result = this.connect
	                             .createStatement( 	ResultSet.TYPE_SCROLL_INSENSITIVE, 
	                                                ResultSet.CONCUR_UPDATABLE)
	                             .executeQuery("SELECT * FROM \"mvc\".licencie WHERE code = '" + code +"'");
	          
	          if(result.first())
	        	  leLicencie = new Licencie(code, result.getString("nom"),result.getString("prenom"),result.getString("dateNaiss"), null);   
	    }
		catch (SQLException e) {
			        e.printStackTrace();
		}
		return leLicencie;
		
	}
		
	
	// Mise à jour d'un licencie
	@Override
	public void update(Licencie obj) {
		try { this .connect	
	               .createStatement( ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		             ResultSet.CONCUR_UPDATABLE )
	               .executeUpdate("UPDATE \"mvc\".licencie SET nom = '" + obj.getNom() + "'"+
	                    	      " WHERE code = '" + obj.getCode()+"'" );
				
			
		}
		catch (SQLException e) {
		      e.printStackTrace();
		}
		

	}


	// Suppression d'un licencie
	@Override
	public void delete(Licencie obj) {
		try {
	           this.connect
	               .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
	            		            ResultSet.CONCUR_UPDATABLE)
	               .executeUpdate("DELETE FROM \"mvc\".licencie WHERE code = '" + obj.getCode()+"'");
				
		 }
		catch (SQLException e) {
		            e.printStackTrace();
		}
	}
}


	


