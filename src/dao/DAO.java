package dao;

import java.sql.Connection;
import java.util.List;

public abstract class DAO<T> {
	
	Connection connect = ConnexionPostgreSql.getInstance();
	
	public abstract void create(T obj); // insérer des données T
 	public abstract T read(String code); // lire code T
	public abstract void update(T obj); // mettre à jour des données T
	public abstract void delete(T obj); // supprimer n'importe quelles données T
	public abstract List<T> recupAll(); // récupère une liste de données T
	
	
		
	
}
